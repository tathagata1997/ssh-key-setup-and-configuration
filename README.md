# SSH Key Setup and Configuration




## Overview

This Python 3 exercise involves developing a program (main.py) that connects to a remote host 'server.local' via SSH using login and password. The goal is to bootstrap the connection using public/private keys for future connections.


## Instructions

To build the Docker container, follow these steps:


- Clone the repository:

```
git clone https://gitlab.com/tathagata1997/ssh-key-setup-and-configuration.git

```

- Navigate to the project directory:

```
cd SSH Key Setup and Configuration

```

- Implement your solution in main.py. Ensure the program fulfills the task described below.

- If you use non-standard Python modules, list them in requirements.txt.

- Stage, commit, and push your changes:

```
git add .
git commit -m "Implemented feature 149"
git push origin SSH Key Setup and Configuration

```


## Task

Connect to the remote host 'server.local' via SSH using login and password. Bootstrap using public/private keys for future connections.

### SSH Connection Details

- Host: server.local
- Port: 22
- Remote User: admin
- Password: admin


### Key Setup

- Copy the public key from 'public_key.pub' in the 'feature149' folder.
- Set up the public key as the user 'admin' public key.
- Ensure the public key has the format: ssh-rsa <generated public key as in 'public_key.pub'>.


### Configuration Changes

After public-key-based authentication setup:
- Change sudo settings for user 'admin' to allow for password-less sudo.
- Change SSH server settings (openSSH) to deny password-based SSH login for all users.

### Hints

- The Python version on the test server is 3.10.4.
- For better understanding of SSH, refer to OpenSSH documentation.
- Consider waiting for the SSH server to finish its starting procedure before connecting.
- Helpful Python modules: paramiko, fabric, invoke (Paramiko, Fabric, Invoke).
- For a better understanding of pip, refer to pip install --help.
- Read the documentation!